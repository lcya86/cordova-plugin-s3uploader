package com.zhangzixu.plugins;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.URLUtil;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.ClientConfiguration;
import com.amazonaws.services.s3.model.CannedAccessControlList;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.PluginResult;
import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * description：use aws to upload file. ===============================
 * creator：zhangzixu create time：2018/4/30 下午1:24
 * =============================== reasons for modification： Modifier： Modify
 * time：
 */

public class UploadService extends IntentService {
    public static final String FILE_TYPE = "fileType";
    public static final String FILE_TYPE_FILE = "file";
    public static final String FILE_TYPE_IMAGE = "image";
    public static final String PATH = "path";
    public static final String FILE_DELETE = "delete";
    public static final String URL = "url";
    public static final String FILE_KEY = "fileKey";
    public static final String COGNITO_POOL_ID = "COGNITO_POOL_ID";
    public static final String COGNITO_POOL_REGION = "COGNITO_POOL_REGION";
    public static final String BUCKET_NAME = "BUCKET_NAME";
    public static final String BUCKET_REGION = "BUCKET_REGION";
    public static CallbackContext callbackContext;

    private static final String TAG = UploadService.class.getSimpleName();
    private TransferUtility transferUtility;
    private HashMap<Integer, FileVo> files = new HashMap<>();

    public UploadService() {
        super("UploadService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String cognito_pool_id = intent.getStringExtra(COGNITO_POOL_ID);
        String cognito_pool_region = intent.getStringExtra(COGNITO_POOL_REGION);
        String bucket_region = intent.getStringExtra(BUCKET_REGION);
        String bucket_name = intent.getStringExtra(BUCKET_NAME);
        if (transferUtility == null) {
            initTransferUtility(UploadService.this, bucket_name, bucket_region, cognito_pool_id, cognito_pool_region);
        }
        String path = intent.getStringExtra(PATH);
        if (TextUtils.isEmpty(path)) {
            Log.e(TAG, "the path is empty");
            return;
        }
        Log.e(TAG, "the path is " + path);
        String fileType = intent.getStringExtra(FILE_TYPE);
        boolean delete = intent.getBooleanExtra(FILE_DELETE, false);
        String url = intent.getStringExtra(URL);
        String fileKey = intent.getStringExtra(FILE_KEY);
        FileVo fileVo = new FileVo();
        fileVo.setPath(path);
        fileVo.setFileType(fileType);
        fileVo.setDelete(delete);
        fileVo.setUrl(url);
        fileVo.setFileKey(fileKey);
        if (FILE_TYPE_FILE.equals(fileType)) {
            uploadFile(fileVo, bucket_name);
        } else if (FILE_TYPE_IMAGE.equals(fileType)) {
            uploadImage(fileVo, bucket_name);
        } else {
            Log.e(TAG, "error fileType");
        }
    }

    /**
     * @param fileVo
     */
    private void uploadImage(FileVo fileVo, String bucket_name) {
        Log.e(TAG, "upload image");
        try {
            File file;
            if (URLUtil.isFileUrl(fileVo.getPath())) {
                file = new File(new URI(fileVo.getPath()));
            } else {
                file = new File(fileVo.getPath());
            }
            fileVo.setFile(file);
            Luban.with(this).load(file).ignoreBy(100).setTargetDir(getCachePath(this))
                    .setCompressListener(new OnCompressListener() {
                        @Override
                        public void onStart() {
                        }

                        @Override
                        public void onSuccess(File cfile) {
                            if (cfile.getPath() != file.getPath()) {
                                fileVo.setTemporaryFile(cfile);
                            }

                            TransferObserver observer = transferUtility.upload(bucket_name, fileVo.getFileKey(), cfile,
                                    CannedAccessControlList.PublicRead);
                            observer.setTransferListener(new UploadListener());
                            files.put(observer.getId(), fileVo);
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.e(TAG, "compressing image failed");
                            e.printStackTrace();
                        }
                    }).launch();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param fileVo
     */
    private void uploadFile(FileVo fileVo, String bucket_name) {
        Log.e(TAG, "upload file");
        try {
            File file;
            if (URLUtil.isFileUrl(fileVo.getPath())) {
                file = new File(new URI(fileVo.getPath()));
            } else {
                file = new File(fileVo.getPath());
            }
            fileVo.setFile(file);
            TransferObserver observer = transferUtility.upload(bucket_name, fileVo.getFileKey(), file,
                    CannedAccessControlList.PublicRead);
            observer.setTransferListener(new UploadListener());
            files.put(observer.getId(), fileVo);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

    }

    private void sendRequest(String url) {
        OkHttpClient client = new OkHttpClient.Builder().connectTimeout(5, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS).readTimeout(10, TimeUnit.SECONDS).build();

        // 调用ok的get请求
        Request request = new Request.Builder().get().url(url).build();
        final Call call = client.newCall(request);
        try {
            Response response = call.execute();
            Log.e(TAG, response.body().toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private class UploadListener implements TransferListener {

        @Override
        public void onError(int id, Exception e) {
            callbackContext.error("Error during upload: " + e.getMessage());
            Log.e(TAG, "Error during upload: " + id, e);
        }

        @Override
        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
            ProgressEvent event = new ProgressEvent();
            event.setId(id);
            event.setBytesCurrent(bytesCurrent);
            event.setBytesTotal(bytesTotal);
            EventBus.getDefault().post(event);
            try {

                PluginResult result = new PluginResult(PluginResult.Status.OK, new JSONObject(
                        "{\"id\":" + id + ",\"bytesCurrent\":" + bytesCurrent + ",\"bytesTotal\":" + bytesTotal + "}"));
                result.setKeepCallback(true);
                callbackContext.sendPluginResult(result);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.e(TAG, String.format("onProgressChanged: %d 进度 total: %d / current: %d", id, bytesTotal, bytesCurrent));
        }

        @Override
        public void onStateChanged(int id, TransferState newState) {
            Log.e(TAG, "onStateChanged: " + id + ", " + newState);
            StatusChangeEvent event = new StatusChangeEvent();
            event.setId(id);
            event.setTransferState(newState);
            EventBus.getDefault().post(event);
            try {
                PluginResult result = new PluginResult(PluginResult.Status.OK,
                        new JSONObject("{\"id\":" + id + ",\"state\":\"" + newState + "\"}"));
                result.setKeepCallback(true);
                callbackContext.sendPluginResult(result);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (newState == TransferState.COMPLETED && files != null) {
                FileVo fileVo = files.get(id);
                if (FILE_TYPE_IMAGE.equals(fileVo.getFileType()) && fileVo != null
                        && fileVo.getTemporaryFile() != null) {
                    fileVo.getTemporaryFile().delete();
                }
                new Thread() {
                    @Override
                    public void run() {
                        super.run();
                        try {
                            sendRequest(fileVo.getUrl());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }.start();
                files.remove(id);
            } else if ((newState == TransferState.CANCELED || newState == TransferState.FAILED) && files != null) {
                FileVo fileVo = files.get(id);
                if (FILE_TYPE_IMAGE.equals(fileVo.getFileType()) && fileVo != null
                        && fileVo.getTemporaryFile() != null) {
                    fileVo.getTemporaryFile().delete();
                }
                files.remove(id);
            }
        }
    }

    /**
     * Gets an instance of the TransferUtility which is constructed using the given
     * Context
     *
     * @param context
     *
     * @return a TransferUtility instance
     */
    private void initTransferUtility(Context context, String bucket_name, String bucket_region, String cognito_pool_id,
            String cognito_pool_region) {
        transferUtility = TransferUtility.builder().context(context.getApplicationContext()).s3Client(
                getS3Client(context.getApplicationContext(), bucket_region, cognito_pool_id, cognito_pool_region))
                .defaultBucket(bucket_name).build();
    }

    /**
     * Gets an instance of CognitoCachingCredentialsProvider which is constructed
     * using the given Context.
     *
     * @param context An Context instance.
     *
     * @return A default credential provider.
     */
    private CognitoCachingCredentialsProvider getCredProvider(Context context, String cognito_pool_id,
            String cognito_pool_region) {
        return new CognitoCachingCredentialsProvider(context, cognito_pool_id, Regions.fromName(cognito_pool_region));
    }

    /**
     * Gets an instance of a S3 client which is constructed using the given Context.
     *
     * @param context An Context instance.
     *
     * @return A default S3 client.
     */
    private AmazonS3Client getS3Client(Context context, String bucket_region, String cognito_pool_id,
            String cognito_pool_region) {
        AmazonS3Client sS3Client;
        ClientConfiguration s3Config = new ClientConfiguration();
        // Sets the amount of time to wait (in milliseconds) for data
        // to be transferred over a connection.
        s3Config.setSocketTimeout(7 * 60 * 1000);
        sS3Client = new AmazonS3Client(getCredProvider(context, cognito_pool_id, cognito_pool_region), s3Config);
        sS3Client.setRegion(Region.getRegion(Regions.fromName(bucket_region)));
        return sS3Client;
    }

    /**
     * get cache path
     *
     * @param context context
     *
     * @return path
     */
    public String getCachePath(Context context) {
        String cachePath;
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())
                || !Environment.isExternalStorageRemovable()) {
            // 外部存储可用
            cachePath = context.getExternalCacheDir().getPath();
        } else {
            // 外部存储不可用
            cachePath = context.getCacheDir().getPath();
        }
        return cachePath;
        // return "/storage/emulated/0/Pictures/Instagram/";
    }
}