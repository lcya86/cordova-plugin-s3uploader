package com.zhangzixu.plugins;

import java.io.File;
import java.io.Serializable;

/**
 * description：
 * ===============================
 * creator：zhangzixu
 * create time：2018/5/3 上午11:52
 * ===============================
 * reasons for modification：
 * Modifier：
 * Modify time：
 */

public class FileVo implements Serializable {
    private String path;
    private String fileType;
    private boolean delete;
    private String url;
    private String fileKey;
    private File file;
    private File temporaryFile;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public boolean isDelete() {
        return delete;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFileKey() {
        return fileKey;
    }

    public void setFileKey(String key) {
        this.fileKey = key;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public File getTemporaryFile() {
        return temporaryFile;
    }

    public void setTemporaryFile(File temporarFile) {
        this.temporaryFile = temporarFile;
    }
}
