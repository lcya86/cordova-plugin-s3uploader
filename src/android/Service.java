package com.zhangzixu.plugins;

import android.content.Intent;
import android.text.TextUtils;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;

public class Service extends CordovaPlugin {

    public CallbackContext callbackContext;
    public String COGNITO_POOL_ID = "";
    public String COGNITO_POOL_REGION = "";
    public String BUCKET_NAME = "";
    public String BUCKET_REGION = "";

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        this.callbackContext = callbackContext;
        if (action.equals("upload")) {
            String path = args.getString(0);
            String fileType = args.getString(1);
            boolean delete = args.getBoolean(2);
            String url = args.getString(3);
            String fileKey = args.getString(4);
            this.upload(path, fileType, delete, url, fileKey, callbackContext);
            return true;
        }else if(action.equals("init")){
            this.COGNITO_POOL_ID = args.getString(0);
            this.COGNITO_POOL_REGION = args.getString(1);
            this.BUCKET_NAME = args.getString(2);
            this.BUCKET_REGION = args.getString(3);
            return true;
        }
        return false;
    }

    /**
     * start service to upload file
     *
     * @param fileType        file type
     * @param callbackContext
     */
    private void upload(String path, String fileType, boolean delete, String url, String fileKey, CallbackContext callbackContext) {
        if (TextUtils.isEmpty(path)) {
            callbackContext.error("path empty!");
            return;
        }
        if (UploadService.FILE_TYPE_IMAGE.equals(fileType) || UploadService.FILE_TYPE_FILE.equals(fileType)) {
            //Toast.makeText(this.cordova.getActivity(), fileType, Toast.LENGTH_SHORT).show();
            Intent i = new Intent(this.cordova.getActivity(), UploadService.class);
            i.putExtra(UploadService.PATH, path);
            i.putExtra(UploadService.FILE_TYPE, fileType);
            i.putExtra(UploadService.FILE_DELETE, delete);
            i.putExtra(UploadService.URL, url);
            i.putExtra(UploadService.FILE_KEY, fileKey);
            i.putExtra(UploadService.COGNITO_POOL_ID, this.COGNITO_POOL_ID);
            i.putExtra(UploadService.COGNITO_POOL_REGION, this.COGNITO_POOL_REGION);
            i.putExtra(UploadService.BUCKET_NAME, this.BUCKET_NAME);
            i.putExtra(UploadService.BUCKET_REGION, this.BUCKET_REGION);
            UploadService.callbackContext = callbackContext;
            this.cordova.getActivity().startService(i);
        } else {
            callbackContext.error("fileType error!");
        }
    }
}