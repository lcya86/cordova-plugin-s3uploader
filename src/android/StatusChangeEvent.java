package com.zhangzixu.plugins;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;

/**
 * description：
 * ===============================
 * creator：zhangzixu
 * create time：2018/5/4 上午8:25
 * ===============================
 * reasons for modification：
 * Modifier：
 * Modify time：
 */

public class StatusChangeEvent {
    private int id;
    private TransferState transferState;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public TransferState getTransferState() {
        return transferState;
    }

    public void setTransferState(TransferState transferState) {
        this.transferState = transferState;
    }
}
