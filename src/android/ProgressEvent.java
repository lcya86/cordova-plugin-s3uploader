package com.zhangzixu.plugins;

/**
 * description：
 * ===============================
 * creator：zhangzixu
 * create time：2018/5/4 上午8:23
 * ===============================
 * reasons for modification：
 * Modifier：
 * Modify time：
 */

public class ProgressEvent {
    private int id;
    private long bytesCurrent;
    private long bytesTotal;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getBytesCurrent() {
        return bytesCurrent;
    }

    public void setBytesCurrent(long bytesCurrent) {
        this.bytesCurrent = bytesCurrent;
    }

    public long getBytesTotal() {
        return bytesTotal;
    }

    public void setBytesTotal(long bytesTotal) {
        this.bytesTotal = bytesTotal;
    }
}
