// cordova.define("cordova-plugin-s3uploader.Service", function(require, exports, module) {
    var exec = require('cordova/exec');

    exports.upload = function (arg0, arg1, arg2, arg3, arg4, success, error) {
        exec(success, error, 'Service', 'upload', [arg0, arg1, arg2, arg3, arg4]);
    };

    exports.init = function (COGNITO_POOL_ID,COGNITO_POOL_REGION,BUCKET_NAME,BUCKET_REGION,success,error){
        exec(success, error, 'Service', 'init',[COGNITO_POOL_ID,COGNITO_POOL_REGION,BUCKET_NAME,BUCKET_REGION]);
    }
// });
