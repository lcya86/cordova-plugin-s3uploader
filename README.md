#Cordova s3Uploader plugin#

##The cordova plugin for LifeVault.##

###Install

```
cordova plugin add https://lcya86@bitbucket.org/lcya86/cordova-plugin-s3uploader.git
```


###Usage

```
cordova.plugins.Service.init(settings.aws_identity_pool_id, settings.aws_region, settings.aws_bucket, settings.aws_region);

cordova.plugins.Service.upload(file.fullPath, 'image', false, toProcessUrl, file_key,function (data) {
  if (data.state) {
    on_status(data.id, data.state);
  } else {
    on_progress(data.id, data.bytesCurrent, data.bytesTotal);
  }
});

function on_progress(id, bytesCurrent, bytesTotal) {
  //get progress
}

function on_status(id, status) {
  //get status
  /**
  * This state represents a transfer that has been queued, but has not yet
  * started
  */
  WAITING,
  /**
  * This state represents a transfer that is currently uploading or
  * downloading data
  */
  IN_PROGRESS,
  /**
  * This state represents a transfer that is paused
  */
  PAUSED,
  /**
  * This state represents a transfer that has been resumed and queued for
  * execution, but has not started to actively transfer data
  */
  RESUMED_WAITING,
  /**
  * This state represents a transfer that is completed
  */
  COMPLETED,
  /**
  * This state represents a transfer that is canceled
  */
  CANCELED,
  /**
  * This state represents a transfer that has failed
  */
  FAILED,

  /**
  * This state represents a transfer that is currently on hold, waiting for
  * the network to become available
  */
  WAITING_FOR_NETWORK,
  /**
  * This state represents a transfer that is a completed part of a multi-part
  * upload. This state is primarily used internally and there should be no
  * need to use this state.
  */
  PART_COMPLETED,
  /**
  * This state represents a transfer that has been requested to cancel, but
  * the service processing transfers has not yet fulfilled this request. This
  * state is primarily used internally and there should be no need to use
  * this state.
  */
  PENDING_CANCEL,
  /**
  * This state represents a transfer that has been requested to pause by the
  * client, but the service processing transfers has not yet fulfilled this
  * request. This state is primarily used internally and there should be no
  * need to use this state.
  */
  PENDING_PAUSE,
  /**
  * This state represents a transfer that has been requested to pause by the
  * client because the network has been loss, but the service processing
  * transfers has not yet fulfilled this request. This state is primarily
  * used internally and there should be no need to use this state.
  */
  PENDING_NETWORK_DISCONNECT,
  /**
  * This is an internal value used to detect if the current transfer is in an
  * unknown state
  */
  UNKNOWN;
}
```
